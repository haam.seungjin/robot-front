import React from 'react';
import ReactDOM from 'react-dom/client';
import { v4 as uuidv4 } from 'uuid';
import './index.css';

// import App from './App';
import Dashboard from './robot/components/Dashboard';
import ShowAlert from './robot/components/ShowAlert'

import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
const simulationId = uuidv4();

root.render(
  <React.StrictMode>
    <Dashboard simulationId={simulationId} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
