import React, { useEffect, useState, useImperativeHandle } from 'react';
import './ShowAlert.css'

const Alert = ({ message, onClose }) => {
  useEffect(() => {
    const timer = setTimeout(() => {
      onClose();
    }, 1500);

    return () => {
      clearTimeout(timer);
    };
  }, [onClose]);

  return (
    <div className="alert">
      <p>{message}</p>
    </div>
  );
};

const ShowAlert = (props, ref) => {
  useImperativeHandle(ref, () => ({
    execChildFunction() {
      makeVisible();
    }
  }));

  const [visible, setVisible] = useState(false);

  const makeVisible = () => {
    setVisible(true);
  };

  const makeUnVisible = () => {
    setVisible(false);
  };

  return (
    <div className='alert-parent'>
      {visible && (
        <Alert message={props.alertObj.data.errorMessage} onClose={makeUnVisible} />
      )}
    </div>
  );
};

export default React.forwardRef(ShowAlert)
// export default ShowAlert;
