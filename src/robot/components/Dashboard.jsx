import React, { useState } from 'react';
import SimulationId from './SimulationId';
import Grid from './Grid';
import MoveList from './MoveList';
import './Dashboard.css';

function Dashboard(props) {
  const [x, setX] = useState(0);
  const [y, setY] = useState(0);
  const [d, setD] = useState('E');

  const handleRobotUpdate = (x, y, d) => {
    setX(x);
    setY(y);
    setD(d);
  }

  return (
    <div className='main-div'>
      <SimulationId simulationId={props.simulationId} />
      <div className='grid-parent'>
        <Grid maxX={5} maxY={5} x={x} y={y} d={d} />
        <MoveList simulationId={props.simulationId} onRobotUpdate={handleRobotUpdate} />
      </div>
    </div>
  );
};

export default Dashboard;
