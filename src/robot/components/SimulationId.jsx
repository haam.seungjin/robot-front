import React, { useState } from 'react';
import './SimulationId.css';

function SimulationId(props) {
  return (
    <div className='simulationId'>
      Simulation Id: {props.simulationId}
    </div>
  );
};

export default SimulationId;
