import React, { useState } from 'react';
import './Robot.css';
import robotIcon from './Robot.png';

function Robot(props) {
  const { x, y, d, maxX, maxY } = props;
  let left = (x * 100 / maxX + 5) + '%';
  let top = (y * 100 / maxX + 4) + '%';
  const deg = () => {
    switch (d) {
      case 'N': return 0;
      case 'E': return 90;
      case 'S': return 180;
      case 'W': return 270;
    }
    return 0;
  }

  return (
    <img className="robot-icon"
      src={robotIcon}
      style={{ left, top, transform: `rotate(${deg()}deg)` }}
      alt='' />
  );
};

export default Robot;
