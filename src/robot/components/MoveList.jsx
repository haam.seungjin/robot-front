import React, { useState, useRef } from 'react';
import { moveApiClient } from '../clients/moveApiClient'
import { historyApiClient } from '../clients/historyApiClient'
import './MoveList.css';
import ShowAlert from './ShowAlert'


function MoveList(props) {
  const [fileContents, setFileContents] = useState([]);
  const [selectedItemIndex, setSelectedItemIndex] = useState(0);
  const [alertObj, setAlertObj] = useState({});
  const ref = useRef();

  const startIndex = Math.max(0, selectedItemIndex - 4);
  const endIndex = Math.min(startIndex + 9, fileContents.length - 1);
  const visibleItems = fileContents.slice(startIndex, endIndex + 1);

  const handleFileUpload = (event) => {
    if (event.target.files.length === 0) {
      return;
    }

    const fileReader = new FileReader();
    fileReader.onload = (event) => {
      setFileContents(event.target.result.split('\n'));
    };
    fileReader.readAsText(event.target.files[0]);
    setSelectedItemIndex(0);
  }

  const handleNextMove = async () => {
    setSelectedItemIndex((prevSelectedItemIndex) =>
      Math.min(prevSelectedItemIndex + 1, fileContents.length)
    );

    const moveCommand = fileContents[selectedItemIndex];
    const result = await moveApiClient(props.simulationId, moveCommand);

    const { data } = result;
    if (data === undefined) return;

    if (data.code === 'ERROR') {
      setAlertObj(result);
      ref.current.execChildFunction();
    }
    props.onRobotUpdate(data.toX, data.toY, data.toD);
  }

  const handleGetHistory = async () => {
    await historyApiClient(props.simulationId);
  }

  return (
    <div className='list-box'>
      <ShowAlert alertObj={alertObj} ref={ref} />
      <ul className='list'>
        {visibleItems.map((item, index) => (
          <li
            key={startIndex + index}
            className={selectedItemIndex === startIndex + index ? 'selected' : ''}
            style={{ backgroundColor: selectedItemIndex === startIndex + index ? 'yellow' : 'transparent' }}
          >
            {item}
          </li>
        ))}
      </ul>
      <input type="file" onChange={handleFileUpload} />
      <button className='btn' onClick={handleNextMove} disabled={fileContents.length === 0 || selectedItemIndex >= fileContents.length}>Move</button>
      <button className='btn' onClick={handleGetHistory}>History</button>
    </div>
  );
}

export default MoveList;
