import React from 'react';
import './Grid.css';
import Robot from './Robot'

function Grid(props) {
  const { x, y, d, maxX, maxY } = props;
  const renderCells = () => {
    const result = [];
    for (let i = 0; i < maxX * maxY; ++i) {
      result.push(
        <div className='cell' key={i}>
          ({Math.floor(i / maxX)}, {i % maxX})
        </div>);
    }
    return result;
  }

  return (
    <div className='grid'>
      {renderCells()}
      <Robot maxX={maxX} maxY={maxY} x={x} y={y} d={d} />
    </div>
  );
};

export default Grid;
