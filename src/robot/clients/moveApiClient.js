import axios from "axios";

const moveApiCall = async (simulationId, moveCommand) => {
  const requestBody = {
    simulationId,
    moveCommand,
  };

  const config = {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer your-token'
    }
  };

  const domain = 'localhost';
  const response = await axios.post(`http://${domain}:8080/v1/move`, requestBody, config);
  console.log('status', response.status);
  console.log('data: ', response.data);
  return response;
}

function moveApiClient(simulationId, moveCommand) {
  return moveApiCall(simulationId, moveCommand).then((result) => {
    return result;
  }).catch((error) => {
    return error;
  });
}

export { moveApiClient };