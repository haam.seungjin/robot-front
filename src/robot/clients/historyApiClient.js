import axios from "axios";

const historyApiCall = async (simulationId) => {
  const domain = 'localhost';
  const response = await axios.get(`http://${domain}:8080/v1/history?simulationId=${simulationId}`);
  console.log('status', response.status);
  console.log('data: ', response.data);
  return response;
}

function historyApiClient(simulationId) {
  return historyApiCall(simulationId).then((result) => {
    return result;
  }).catch((error) => {
    return error;
  });
}

export { historyApiClient };